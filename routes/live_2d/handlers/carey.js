const path = require('path');
const root = path.parse(process.mainModule.filename).dir;
const express = require('express');
const router = express.Router();
const fs = require('fs');

// Redis Coonect Checking
const handlers = {
    carey: carey
};

async function carey(req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.sendFile(path.join(__dirname+'/carey.html'))
}

module.exports = handlers;