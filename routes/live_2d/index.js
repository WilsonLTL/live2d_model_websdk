const path = require('path');
const root = path.parse(process.mainModule.filename).dir;
const express = require('express');
const router = express.Router({ mergeParams: true });

const handler = {
    carey: require("./handlers/carey").carey,
    carey2: require("./handlers/carey2").carey2
};

router.route("/carey").get(handler.carey);
router.route("/carey2").get(handler.carey2);


module.exports = router;