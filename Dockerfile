FROM node:latest
COPY . /code
WORKDIR /code
RUN npm install
EXPOSE 5000
CMD ["node","app.js"]