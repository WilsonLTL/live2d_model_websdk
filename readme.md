# live2D_model_webSDK
## A micro service of Live2D
![image](https://i.imgur.com/cpJQm7l.png)

## Setting
```text
1. sudo apt-get update -qy
2. sudo apt install npm -qy
3. cd into repo, npm install
4. node app.js
5. Enter http://localhost:5000/live_2d/carey, in console, enter "live2Drunner(0,2.0,2.5)"

```

## API Guide
1. Call Carey Model <br >
Url: http://localhost:5000/live_2d/carey <br >
Method: GET <br >
Response: Carey.html

2. Control motion <br >
Cmd: document.getElementById("frameid").contentWindow.postMessage('tapBody:actionid')

3. Control frame size <br >
Cmd: document.getElementById("frameid").contentWindow.postMessage('live2Drunner:centex_x:y:width')

3. Stop motion <br >
Cmd: document.getElementById("frameid").contentWindow.postMessage('stopMotion')

## Import new Live2D model
```text
1. In public/assets/live2d/, touch a new folder with the name of new model.
2. Paste the texture file, motion file .moc file, model JSON and pose JSON in to folder.
e.g create a new folder "carey2", paste texture in "carey2/texture", paste .moc file and model.json
3. In model JSON, please define the textures location, model location, motions location, pose location etc. 
PS1: For example, please view carey_NEW model.json.
4. In src, copy carey and paste for a new one, rename the folder name into new model name.
5. In src/LAppDefine.js, in line 31, add MODEL_{new_model_name} and define the location of the model JSON 
6. In {new_model_name}/LAppLive2DManager.js, In line 45, modify "this.models[0].load(gl, LAppDefine.MODEL_CAREY_NEW);",MODEL_CAREY_NEW => MODEL_{new_model_name}
e.g. MODEL_CAREY_NEW: "../static/assets/live2d/carey_NEW/haru__216_363_225t_203X_201[_203c__203C_203_223_203__201[_203g_t08.model.json",
PS2: For example, please view  MODEL_CAREY_NEW
7. In routes/live_2d/handlers,copy carey.html and carey.js and paste as "{new_model_name}.html" and "{new_model_name}.js", modify {new_model_name}.js
8. In {new_model_name}.html modify line 85, modify the target line in to the src/{new_model_name}/LAppLive2DManager.js
```